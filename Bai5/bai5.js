/*
input: nhap so co 2 chu so

todo:
- tao bien nhap so co 2 chu so
- tinh hang don vi: donVi = n % 10
- tinh hang chuc: hangChuc = Math.floor(n/10)%10

ouput: result = donVi + hangChuc  */

var n;
var donVi;
var hangChuc;
var result;

n = 26
donVi = n % 10;
hangChuc = Math.floor(n / 10) % 10;
result = hangChuc + donVi;
console("result: ", result);